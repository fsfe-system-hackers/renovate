---
title: Renovate
description: How to use renovate for your project
---

Renovate  scans the repositories for outdated dependencies and after finding any, it automatically creates a PR with the update. It also creates a new issue(Dependency Dashboard) within the project repository. This issue lists Renovate updates and detected dependencies.


## Start with renovate

- If you want to add a repository to renovate, simply click `Manage Topics` in a repository and  add renovate.
- After that you should restart the last build of renovate from the [drone](https://drone.fsfe.org/fsfe-system-hackers/renovate/913) to trigger its run, unless you want to just wait till 01:00 am , when renovate will run according to its schedule.
- Renovate will create a first PR, suggesting you to add preconfigured `renovate.json` file.
:warning: If the project you just added renovate is REUSE compliant, you should also add `renovate.json.license` file in a project repository manualy.
- After merging the PR , you should restart the last build of renovate once again, to trigger it to check your repository for updates and to create a new issue(Dependency Dashboard).

## Versioning for docker images

It can be challenging for Renovate to detect dependencies for Docker images. This difficulty arises from the [tag-based](https://docs.docker.com/engine/reference/commandline/tag/) versioning system used on Docker Hub. Renovate is using [SemVer](https://semver.org/) as default and it works fine for docker images in most of the cases. However if you don't see a particular docker image in dependency dashboard , it means that you should explicitaly define the versioning strategy. The strategy that is used in most of our repos is a custom [regex versioning scheme.](https://docs.renovatebot.com/modules/versioning/#regular-expression-versioning) To build, test or debug the regex, you can use [this website](https://regex101.com/).
:warning: on the website you use a single backslash, but in a `renovate.json` file double backslashes are needed. Below is the example of the regex , for our passbolt image versioning, which corresponds the `passbolt/passbolt:4.0.2-1-pro` image.

```

  "packageRules": [
    {
      "matchDatasources": ["docker"],
      "matchPackageNames": ["passbolt/passbolt"],
      "versioning": "regex:^(?<major>\\d+)(\\.(?<minor>\\d+))?(\\.(?<patch>\\d+))?(-(?<build>\\d+))?(-(?<compatibility>pro))$"
    }
  ]


```

Regex in our example is broken down like this:

`major`	-	4
`minor`	-	0
`patch`	-	2
`build`	-	1
`compatibility`	-	this value is used , for the part which we don't want to be updated. In this example, renovate will only makes a new PR  for the newer tags with the prefix `-pro`.


## Range Strategy

Renovate also provides [rangeStrategy,](https://docs.renovatebot.com/configuration-options/#rangestrategy) to define version ranges for your dependencies. This strategy enables you to specify the acceptable range of versions for a particular dependency, allowing Renovate to automatically update to newer versions within that range. This guaranties that your dependencies stay up to date while maintaining compatibility with your project. By using Range Strategy, you can easily manage and control the versioning of your dependencies in Renovate. Same as versioning, range strategy should be defined in `"packageRules"`.

## Reduce the noise

Some times renovate is becomming annoyng with its automatic PRs. The Dependency Dashboard introduces an "approval" workflow for new upgrades. It means, that you can configure your projects repo so, that you will be able to choose to approve updates for specific dependencies (which is recommended) or even for all dependencies. So, if you want to opt-out of getting automatic updates whenever they're ready, you can tell Renovate to wait for your approval before making any pull requests. Read the Dependency Dashboard official [docs](https://docs.renovatebot.com/key-concepts/dashboard/#dependency-dashboard-approval-workflow) to learn more.
