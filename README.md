# Automatic dependency updates

[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/renovate/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/renovate)

All this is, is a simple [.drone.yml](.drone.yml) file that runs the
[renovate/renovate](https://hub.docker.com/r/renovate/renovate) container as the
user [Renovate Bot](https://git.fsfe.org/renovate-bot?tab=activity).

Renovate scours through the dependencies of any given project and sees whether
there have been any updates. If there are, it automatically writes a PR with
the changelog of the new version as a description and the actual change in
versions. So all you need to do after reviewing the PR is to merge it and your
project's dependencies are updated.

If you want to add a repository to renovate, simply add the topic `renovate`.
For fine-tuning renovate's interaction with your project, please see the
[documentation](https://docs.renovatebot.com/configuration-options/).

:warning: If the project you just added renovate is REUSE compliant, you also should add
`renovate.json.license` file in a project repo.

Perhaps the most important settings are: 
[`rangeStrategy`](https://docs.renovatebot.com/configuration-options/#rangestrategy)
and [`versioning`](https://docs.renovatebot.com/modules/versioning) which determines
how renovate updates or modifies existing version ranges in the given repo.

We set up renovate to run daily at 1AM. If you want to trigger a run manually,
simply go to our [Drone
interface](https://drone.fsfe.org/fsfe-system-hackers/renovate) and restart the
latest build.

See [Renovate's docs](https://docs.renovatebot.com/self-hosted-configuration/)
for more information on our self-hosted setup.

## Editing this repository
This repository should only be pushed to by the `renovate-bot`, because the commits
in other repos are always done by the last account who has committed to this
repository. Who knows why...

## Local testing

To run the pipeline locally, you'll have to install `drone-cli`. You should also
create a file for the secrets that you can inject in the pipeline, just as the
Drone server takes it from its keystore. Just copy `,env.sample` to `.env` and
enter its secret values.

Then run: `drone exec --event push --branch master --secret-file .env --trusted`

