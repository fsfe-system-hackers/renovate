const platform = "gitea";
const endpoint = "https://git.fsfe.org/api/v1/";
const token = process.env.RENOVATE_TOKEN;

// parse the repos.json file created by the `curl` command in .drone.yml
const repoJson = require("/repos/repos.json");
const repositories = repoJson.data.map((r) => r.full_name);

module.exports = {
  platform,
  token,
  endpoint,
  repositories,
  // Enable beta functionality of updating git submodules
  "git-submodules": {
    "enabled": true
  },
  onboardingConfig: {
    "extends": [
      "config:recommended"
    ]
  },
};
